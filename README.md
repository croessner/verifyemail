# About

*verifyemail* is a Postfix milter for submission instances.

It compares the MAIL FROM from the SMTP session with the From:-header of
outbound messages. If the addresses match, sending is permitted.

## Features

The current implementation does the following checks:

* More than one From:-header will cause a reject (reason: RFC violation)
* More than one e-mail address is listed in the From:-header. This is a 
  limitation and will probably change in the future by adding a header.
  Currently the result ends up in a reject.

## Installation

Install libmilter and the python bindings (often known as pymilter). Place the
verifyemail script to /usr/local/sbin. Place the systemd unit file to
/etc/systemd/system. Call

```
systemctl daemon-reload
systemctl enable verifyemail
systemctl start verifyemail
```

The milter will listen on port 30072 at 127.0.0.1. Add your milter in Postfix
to the smtpd_milters parameter:

```
smtpd_milters = ..., inet:127.0.0.1:30072, ...
```

Make sure the milter ONLY runs on submission instances!

The milter has a dry-run mode, which is activated by adding the parameter

```
--action accept
```

If you need a different port, use the following option:

```
--socket inet:port@ipv4
--socket inet6:port@ipv6
--socket unix:/path/to/socket
```

You will need to adjust the systemd unit file for changes to take effect.

You can also change the syslog facility and priority. Have a look for the
setting by calling

```
verifyemail --help
```

## Testing

If you have installed *miltertest* from the OpenDKIM project, you can run the
tests from the tests/folder by simply calling the testing.sh script on a shell.

Enjoy
