FROM python:3-slim

LABEL org.opencontainers.image.authors="christian@roessner.email"

WORKDIR /usr/app

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get -qq install python3-milter \
    && rm -rf /var/lib/apt/lists/* \
    && useradd -rMs /sbin/nologin milter

COPY ./verifyemail ./

CMD ["./verifyemail", "--socket", "inet:30072@0.0.0.0", "--stdout"]