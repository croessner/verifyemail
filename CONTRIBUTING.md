Many thanks to the people from mail.de GmbH:
  - Code review
  - Testing
  - Fixes

Code author:
  - Christian Rößner <christian@roessner.email>
